import { ClientController, ProjectController } from './controller';
import { SwaggerRouter } from 'koa-swagger-decorator';

const router = new SwaggerRouter({ prefix: '/api/v1'});

/** CLIENTS */
router.get('/clients', ClientController.getClients);
router.post('/clients', ClientController.createClient);
router.put('/clients/:id', ClientController.updateClient);
router.delete('/clients/:id', ClientController.deleteClient);

/** PROJECTS */
router.get('/clients/:clientId/projects', ProjectController.getProjectsByClient);
router.post('/clients/:clientId/projects', ProjectController.createProjectForClient);
router.get('/projects', ProjectController.getProjects);
router.get('/projects/:id', ProjectController.createProject);

router.swagger({
    title: 'Projects Microservice',
    description: 'API REST to manage Projects, Clients and User assignments',
    version: '0.0.1'
});

/** mapDir will scan the input dir, and automatically call router.map to all Router Class */
router.mapDir(__dirname);

export { router as ApiRouter };