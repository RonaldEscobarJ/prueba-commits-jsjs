import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';

import { Project } from './project.entity';

@Entity()
export class Client {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @OneToMany(type => Project, project => project.client)
    projects: Project[];
}

export const ClientSchema = {
    name: { type: 'string', required: true, example: 'Client-Name' }
};