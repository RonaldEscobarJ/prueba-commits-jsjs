import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';

import { Client } from './client.entity';

@Entity()
export class Project {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @ManyToOne(type => Client, client => client.projects)
    client: Client;
}

export const ProjectSchema = {
    name: { type: 'string', required: true, example: 'Project-A'},
};
