import { BaseContext } from 'koa';
import { getManager, Repository } from 'typeorm';
import { tagsAll, responsesAll, request, summary, body, path } from 'koa-swagger-decorator';

import { HttpResponses } from './_responses';
import { Project, ProjectSchema } from '../entity/project.entity';
import { Client } from '../entity/client.entity';
import { ValidationError, validate } from 'class-validator';

@responsesAll(HttpResponses)
@tagsAll(['Project'])
export default class ProjectController {

    @request('get', '/projects')
    @summary('Get all registred projects')
    public static async getProjects(ctx: BaseContext) {
        const projectRepository: Repository<Project> = getManager().getRepository(Project);
        const projects: Project[] = await projectRepository.find({ relations: ['client'] });
        ctx.status = 200;
        ctx.body = projects;
    }

    @request('post', '/projects')
    @summary('Add new project')
    @body(ProjectSchema)
    public static async createProject(ctx: BaseContext) {
        const projectRepository: Repository<Project> = getManager().getRepository(Project);
        const projectToBeSaved: Project = new Project();
        projectToBeSaved.name = ctx.request.body.name;
        const response: Project = await projectRepository.save(projectToBeSaved);
        ctx.status = 200;
        ctx.body = response;
    }

    @request('get', '/clients/{clientId}/projects')
    @summary('Get all projects that belongs to given client')
    @path({
        clientId: { type: 'number', required: true, description: 'client identifier' }
    })
    public static async getProjectsByClient(ctx: BaseContext) {
        const clientRepository: Repository<Client> = getManager().getRepository(Client);
        const client: Client = await clientRepository.findOne(+ctx.params.clientId || 0, { relations: ['projects'] });
        // @TODO Add try/catch in order to get the exception when a client doesn't have projects
        const projects: Project[] = client.projects || [];
        ctx.status = 200;
        ctx.body = projects;
    }

    @request('post', '/clients/{clientId}/projects')
    @summary('Creates a new project for given client')
    @path({
        clientId: { type: 'number', required: true, description: 'client identifier' }
    })
    @body(ProjectSchema)
    public static async createProjectForClient(ctx: BaseContext) {
        const clientRepository: Repository<Client> = getManager().getRepository(Client);
        const projectRepository: Repository<Project> = getManager().getRepository(Project);
        const client: Client = await clientRepository.findOne(+ctx.params.clientId || 0);
        const projectToBeSaved: Project = new Project();
        projectToBeSaved.name = ctx.request.body.name;
        projectToBeSaved.client = client;
        const errors: ValidationError[] = await validate(projectToBeSaved);

        if (errors.length > 0) {
            ctx.status = 400;
            ctx.body = errors;
        } else {
            const project = await projectRepository.save(projectToBeSaved);
            ctx.status = 201;
            ctx.body = project;
        }
    }
}