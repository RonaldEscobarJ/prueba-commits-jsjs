import { BaseContext } from 'koa';
import { getManager, Repository } from 'typeorm';
import { validate, ValidationError } from 'class-validator';
import { tagsAll, responsesAll, request, summary, body, path } from 'koa-swagger-decorator';

import { HttpResponses } from './_responses';
import { Client, ClientSchema } from '../entity/client.entity';

@responsesAll(HttpResponses)
@tagsAll(['Client'])
export default class ClientController {

    @request('get', '/clients')
    @summary('Get all registered clients')
    public static async getClients(ctx: BaseContext) {
        const clientRepository: Repository<Client> = getManager().getRepository(Client);
        const users: Client[] = await clientRepository.find();
        ctx.status = 200;
        ctx.body = users;
    }

    @request('get', '/clients/{id}')
    @summary('Gets client information')
    @path({
        id: { type: 'number', required: true, description: 'client identifier' }
    })
    public static async getClient(ctx: BaseContext) {
        const clientRepository: Repository<Client> = getManager().getRepository(Client);
        const client: Client = await clientRepository.findOne(+ctx.params.id || 0);
        ctx.status = 200;
        ctx.body = client;
    }

    @request('post', '/clients')
    @summary('Creates a new client')
    @body(ClientSchema)
    public static async createClient(ctx: BaseContext) {
        const clientRepository: Repository<Client> = getManager().getRepository(Client);
        const clientToBeSaved: Client = new Client();
        clientToBeSaved.name = ctx.request.body.name;
        const errors: ValidationError[] = await validate(clientToBeSaved); // errors is an array of validation errors

        if (errors.length > 0) {
            ctx.status = 400;
            ctx.body = errors;
        } else {
            const user = await clientRepository.save(clientToBeSaved);
            ctx.status = 201;
            ctx.body = user;
        }
    }

    @request('put', '/clients/{id}')
    @summary('Updates client information')
    @path({
        id: { type: 'number', required: true, description: 'client identifier' }
    })
    @body(ClientSchema)
    public static async updateClient(ctx: BaseContext) {
        const clientRepository: Repository<Client> = getManager().getRepository(Client);
        const clientToBeUpdated: Client = new Client();
        clientToBeUpdated.id = +ctx.params.id || 0;
        clientToBeUpdated.name = ctx.request.body.name;
        const errors: ValidationError[] = await validate(clientToBeUpdated);

        if (errors.length > 0) {
            ctx.status = 400;
            ctx.body = errors;
        } else if (!await clientRepository.findOne(clientToBeUpdated.id)) {
            ctx.status = 400;
            ctx.body = 'The client you are trying to update doesn\'t exist in the db';
        } else {
            const user = await clientRepository.save(clientToBeUpdated);
            ctx.status = 201;
            ctx.body = user;
        }

    }

    @request('delete', '/clients/{id}')
    @summary('Deletes client by id')
    @path({
        id: { type: 'number', required: true, description: 'client id' }
    })
    public static async deleteClient(ctx: BaseContext) {
        const clientRepository = getManager().getRepository(Client);
        const clientToRemove: Client = await clientRepository.findOne(+ctx.params.id || 0);
        if (!clientToRemove) {
            ctx.status = 400;
            ctx.body = 'The user you are trying to delete doesn\'t exist in the db';
        } else {
            await clientRepository.remove(clientToRemove);
            ctx.status = 204;
        }

    }
}