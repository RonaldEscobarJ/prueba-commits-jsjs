# Projects Microservice

## Setup

In order to have project running you just need to run following commands

NOTE: First rename the current `.example.env` file to `.env` (this file contains the DB configuration), then run the following commands:

```
$ npm install
```

then run the development server

```
$ npm run watch-server
```

By default the project is deployed under http://localhost:3000/api/v1/swagger-html

Currently the project uses Koa and Postgresql, so in order to avoid some installations there is already a docker compose file that you can use in order to start playing with the application, along with that the compose files gives you the admin tool(`adminer`) to review the DB (http://localhost:8080/)

In order to start the docker, you just need to execute the following command

```
$ docker-compose up
```
This will create two containers, one for `postgresql` and another one for `adminer`.

You can review the running containers using the following command

```
docker container ls -aq
```

If you want to know which user and database we are using, you can go to `docker-compose.yml` and/or `.env`.